Source: golang-github-d2r2-go-bsbmp
Maintainer: Debian Go Packaging Team <team+pkg-go@tracker.debian.org>
Uploaders: Benjamin Drung <bdrung@debian.org>
Section: devel
Testsuite: autopkgtest-pkg-go
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-golang,
               golang-any,
               golang-github-d2r2-go-i2c-dev,
               golang-github-d2r2-go-logger-dev
Standards-Version: 4.6.0
Vcs-Browser: https://salsa.debian.org/go-team/packages/golang-github-d2r2-go-bsbmp
Vcs-Git: https://salsa.debian.org/go-team/packages/golang-github-d2r2-go-bsbmp.git
Homepage: https://github.com/d2r2/go-bsbmp
Rules-Requires-Root: no
XS-Go-Import-Path: github.com/d2r2/go-bsbmp

Package: golang-github-d2r2-go-bsbmp-dev
Architecture: all
Multi-Arch: foreign
Depends: golang-github-d2r2-go-i2c-dev,
         golang-github-d2r2-go-logger-dev,
         ${misc:Depends}
Description: interact with Bosch Sensortec BMP180/BMP280/BME280/BMP388 sensors (library)
 Digital humidity and pressure sensors from Bosch Sensortec are popular among
 Arduino and Raspberry Pi developers. These sensors are compact and quite
 accurately measuring, working via Inter-Integrated Circuit (I2C) bus interface.
 .
 This library is written in Go programming language for Raspberry Pi and
 counterparts. It makes all necessary i2c-bus interactions and values
 computation to gives you the temperature and atmospheric pressure values. BME
 sensors also provide relative humidity values.
 .
 Following sensors from Bosch Sensortec are supported:
  * BMP180 (pressure and temperature)
  * BMP280 (pressure and temperature)
  * BME280 (humidity, pressure, and temperature)
  * BMP388 (pressure and temperature)
